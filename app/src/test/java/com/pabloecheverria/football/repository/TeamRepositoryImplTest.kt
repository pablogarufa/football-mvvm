package com.pabloecheverria.football.repository

import arrow.core.Left
import arrow.core.Right
import com.nhaarman.mockitokotlin2.*
import com.pabloecheverria.football.base.custom.State
import com.pabloecheverria.football.base.mapper.Mapper
import com.pabloecheverria.football.base.network.FirestoreDataSource
import com.pabloecheverria.football.base.network.FootballService
import com.pabloecheverria.football.feature.teams.TeamRepositoryImpl
import com.pabloecheverria.football.feature.teams.db.Team
import com.pabloecheverria.football.feature.teams.db.TeamDao
import com.pabloecheverria.football.feature.teams.network.TeamDTO
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TeamRepositoryImplTest {

    private val teamDao: TeamDao = mock()
    private val footballService: FootballService = mock()
    private val mapper: Mapper<TeamDTO, Team> = mock()
    private val firestoreDataSource: FirestoreDataSource = mock()

    private lateinit var repository: TeamRepositoryImpl

    @Before
    fun setup() {
        repository = TeamRepositoryImpl(mapper, teamDao, footballService, firestoreDataSource)
    }

    @Test
    fun `observe teams with empty database SHOULD return no results`() =
        runBlockingTest {

            whenever(teamDao.getTeams(leagueID)) doReturn flowOf(emptyList())

            val flowResult = repository.observeTeams(leagueID)

            assertEquals(0, flowResult.toList().size)
        }

    @Test
    fun `observe teams SHOULD return team results`() =
        runBlockingTest {

            val teams: List<Team> = mock()

            whenever(teamDao.getTeams(leagueID)) doReturn flowOf(teams)

            val flowResult = repository.observeTeams(leagueID)

            assertEquals(1, flowResult.toList().size)
            assertTrue(flowResult.toList()[0] is State.Success)
            assertEquals(teams, flowResult.toList()[0].data)
        }

    @Test
    fun `get teams SHOULD return loading state THEN add results to the database`() =
        runBlockingTest {

            val team: Team = mock()
            val teamsDTO: List<TeamDTO> = mock()
            val firestoreTeams: List<Team> = mock()
            val mappedTeams: List<Team> = listOf(team)

            whenever(mapper.mapList(teamsDTO)) doReturn mappedTeams
            whenever(footballService.getTeams(leagueID)) doReturn Right(teamsDTO)
            whenever(firestoreDataSource.getTeams(leagueID)) doReturn Right(firestoreTeams)

            val flowResult = repository.getTeams(leagueID)

            val listResult = flowResult.toList()

            assertEquals(1, listResult.size)
            assertTrue(listResult[0] is State.Loading)

            verify(teamDao, times(2)).addTeams(any())
        }

    @Test
    fun `GIVEN server issues WHEN get teams SHOULD return loading state THEN error state`() =
        runBlockingTest {

            val teams: List<Team> = mock()

            whenever(teamDao.getTeams(leagueID)) doReturn flowOf(teams)
            whenever(footballService.getTeams(leagueID)) doReturn Left(State.Error.NetworkError())
            whenever(firestoreDataSource.getTeams(leagueID)) doReturn Left(State.Error.NetworkError())

            val flowResult = repository.getTeams(leagueID)

            val listResult = flowResult.toList()

            assertEquals(2, listResult.size)
            assertTrue(flowResult.toList()[0] is State.Loading)
            assertTrue(flowResult.toList()[1] is State.Error)
            assertEquals(teams, listResult[1].data)
        }

    @Test
    fun `add a team SHOULD add it into the database`() =
        runBlockingTest {
            val teamName = "Juventus"
            val team: Team = Team(teamsId = "123", name = teamName, leagueId = leagueID)

            whenever(firestoreDataSource.addTeam(teamName, leagueID)) doReturn Right(team)

            repository.addTeam(teamName, leagueID)

            verify(teamDao).addTeam(team)
        }

    @Test
    fun `delete a team SHOULD delete it into the database`() =
        runBlockingTest {
            val team: Team = mock()
            repository.deleteTeam(team)

            verify(teamDao).deleteTeam(team)
        }

    companion object {
        private const val leagueID = 1
    }
}