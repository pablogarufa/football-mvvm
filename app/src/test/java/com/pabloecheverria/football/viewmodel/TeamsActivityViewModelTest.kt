package com.pabloecheverria.football.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.*
import com.pabloecheverria.football.base.custom.State
import com.pabloecheverria.football.feature.teams.TeamRepository
import com.pabloecheverria.football.feature.teams.TeamsActivityViewModel
import com.pabloecheverria.football.feature.teams.db.Team
import com.pabloecheverria.football.util.CoroutinesTestRule
import com.pabloecheverria.football.util.getOrAwaitValue
import com.pabloecheverria.football.util.observeForTesting
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.test.assertEquals

class TeamsActivityViewModelTest {

    private val teamRepository: TeamRepository = mock()
    private lateinit var teamsActivityViewModel: TeamsActivityViewModel
    private val observer: Observer<State<List<Team>>> = mock()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Before
    fun setup() {
    }

    @Test
    fun `WHEN access the teams scren THEN loads current teams`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {

            val loadingState = State.Loading<List<Team>>()
            val successState = State.Success<List<Team>>(emptyList())

            whenever(teamRepository.getTeams(leagueID)) doReturn flow {
                emit(loadingState)
            }
            whenever(teamRepository.observeTeams(leagueID)) doReturn flow {
                delay(20000)
                emit(successState)
            }

            teamsActivityViewModel =
                TeamsActivityViewModel(leagueID, teamRepository, coroutinesTestRule.testDispatcher)

            verify(teamRepository).observeTeams(leagueID)
            verify(teamRepository).getTeams(leagueID)
        }

    companion object {
        private const val leagueID = 1
    }
}

