package com.pabloecheverria.football.feature.teams

import com.pabloecheverria.football.base.custom.State
import com.pabloecheverria.football.base.mapper.Mapper
import com.pabloecheverria.football.base.network.FirestoreDataSource
import com.pabloecheverria.football.base.network.FootballService
import com.pabloecheverria.football.feature.teams.db.Team
import com.pabloecheverria.football.feature.teams.db.TeamDao
import com.pabloecheverria.football.feature.teams.network.TeamDTO
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.*


class TeamRepositoryImpl(
    private val mapper: Mapper<TeamDTO, Team>,
    private val teamDao: TeamDao,
    private val footballService: FootballService,
    private val firestoreDataSource: FirestoreDataSource
) : TeamRepository {

    override fun observeTeams(leagueId: Int): Flow<State<List<Team>>> {
        return teamDao.getTeams(leagueId).filter { it.isNotEmpty() }.map { State.Success(it) }
    }

    @ExperimentalCoroutinesApi
    override fun getTeams(leagueId: Int) = flow<State<List<Team>>> {
        coroutineScope {
            val deferredApiResult = async { footballService.getTeams(leagueId) }
            val deferredFirestoreResult = async { firestoreDataSource.getTeams(leagueId) }

            var errorEmitted = false

            deferredApiResult.await().fold({
                errorEmitted = true
                emit(it.apply { data = teamDao.getTeams(leagueId).first() })
            }, {
                val mappedTeams = mapper.mapList(it).map { it.apply { it.leagueId = leagueId } }
                teamDao.addTeams(mappedTeams)
            })
            deferredFirestoreResult.await().fold({
                if (!errorEmitted) {
                    emit(it.apply { data = teamDao.getTeams(leagueId).first() })
                }
            }, {
                teamDao.addTeams(it)
            })
        }
    }.onStart {
        emit(State.Loading())
    }.onEach {
        println("Item emitted: $it")
    }

    override suspend fun addTeam(teamName: String, leagueId: Int) {
        firestoreDataSource.addTeam(teamName, leagueId).fold({
            // TODO()
        }, {
            teamDao.addTeam(it)
        })
    }

    override suspend fun deleteTeam(team: Team) {
        teamDao.deleteTeam(team)
    }
}