package com.pabloecheverria.football.feature.teams

import com.pabloecheverria.football.base.mapper.Mapper
import com.pabloecheverria.football.feature.teams.db.Team
import com.pabloecheverria.football.feature.teams.network.TeamDTO

class TeamMapper : Mapper<TeamDTO, Team> {

    override fun map(input: TeamDTO): Team {
        return Team(teamsId = input.team_team_key.toString(), name = input.team_name, badgeUrl = input.team_badge)
    }
}