package com.pabloecheverria.football.feature.leagues

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.pabloecheverria.football.base.custom.State
import com.pabloecheverria.football.feature.leagues.db.League
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class LeaguesActivityViewModel @ViewModelInject constructor(
    private val repository: LeagueRepository,
    private val dispatcher: CoroutineDispatcher
) : ViewModel() {

    init {
        observeLeagues()
        requestLeagues()
    }

    private val _leagues = MutableLiveData<State<List<League>>>()
    val leagues: LiveData<State<List<League>>> = _leagues

    private fun observeLeagues() {
        viewModelScope.launch(dispatcher) {
            repository.observeLeagues().collect {
                _leagues.postValue(State.Success(it))
            }
        }
    }

    fun requestLeagues() {
        viewModelScope.launch(dispatcher) {
            repository.getLeagues().collect {
                _leagues.postValue(it)
            }
        }
    }
}
