package com.pabloecheverria.football.feature.leagues.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "leagues")
data class League(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "leagueId")
    val leagueId: Int = 0,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "badgeUrl")
    val badgeUrl: String? = null
) {
    override fun toString(): String {
        return "$leagueId  $name"
    }
}