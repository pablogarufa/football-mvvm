package com.pabloecheverria.football.feature.teams

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlinx.coroutines.CoroutineDispatcher

class TeamsViewModelFactory(
    private val leagueId: Int,
    private val repository: TeamRepository,
    private val dispatcher: CoroutineDispatcher
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TeamsActivityViewModel(leagueId, repository, dispatcher) as T
    }
}