package com.pabloecheverria.football.feature.leagues.db

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface LeagueDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addLeague(league: League)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addLeagues(leagues: List<League>)

    @Update
    suspend fun updateLeague(league: League)

    @Update
    suspend fun updateLeagues(leagues: List<League>)

    @Delete
    suspend fun deleteLeague(league: League)

    @Query("SELECT * from leagues ORDER BY name ASC")
    fun getLeagues(): Flow<List<League>>

    @Query("DELETE FROM leagues")
    suspend fun deleteAll()
}