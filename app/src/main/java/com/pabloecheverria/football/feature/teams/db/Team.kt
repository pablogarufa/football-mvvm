package com.pabloecheverria.football.feature.teams.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "teams"/*,
    foreignKeys = [ForeignKey(
        entity = League::class,
        parentColumns = ["leagueId"],
        childColumns = ["teamsId"]
    )]*/
)
data class Team(
    @PrimaryKey
    @ColumnInfo(name = "teamsId")
    val teamsId: String,
    @ColumnInfo(name = "leagueId")
    var leagueId: Int = 0,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "badgeUrl")
    val badgeUrl: String? = null
) {
    override fun toString(): String {
        return "$teamsId  $name"
    }
}