package com.pabloecheverria.football.feature.leagues

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.pabloecheverria.football.R
import com.pabloecheverria.football.base.custom.State
import com.pabloecheverria.football.base.extension.toast
import com.pabloecheverria.football.feature.leagues.db.League
import com.pabloecheverria.football.feature.teams.TeamsActivity
import com.pabloecheverria.football.feature.teams.TeamsActivity.Companion.EXTRA_LEAGUE_BADGE_URL
import com.pabloecheverria.football.feature.teams.TeamsActivity.Companion.EXTRA_LEAGUE_ID
import com.pabloecheverria.football.feature.teams.TeamsActivity.Companion.EXTRA_LEAGUE_TITLE
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_leagues.*
import javax.inject.Inject

@AndroidEntryPoint
class LeaguesActivity : AppCompatActivity(), LeagueListAdapter.Interaction {

    private val viewModel by viewModels<LeaguesActivityViewModel>()

    private lateinit var leagueListAdapter: LeagueListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leagues)
        title = getString(R.string.title_leagues)
        initRecyclerView()
        setupSwipeRefresh()
        subscribeObservers()
    }

    private fun subscribeObservers() {
        lifecycleScope.launchWhenStarted {
            viewModel.leagues.observe(this@LeaguesActivity, Observer {
                println("GARUFA: observe $it")
                when (it) {
                    is State.Success -> handleSuccess(it.data)
                    is State.Error -> handleError(it.data)
                    is State.Loading -> handleLoading()
                }
            })
        }
    }

    private fun initRecyclerView() {
        recycler_leagues.apply {
            layoutManager = LinearLayoutManager(this@LeaguesActivity)
            leagueListAdapter = LeagueListAdapter(this@LeaguesActivity)
            adapter = leagueListAdapter
        }
    }

    private fun setupSwipeRefresh() {
        swipe_refresh.setOnRefreshListener {
            viewModel.requestLeagues()
        }
    }

    private fun handleLoading() {
        swipe_refresh.isRefreshing = true
    }

    private fun handleSuccess(leagues: List<League>?) {
        swipe_refresh.isRefreshing = false
        renderLeagues(leagues)
    }

    private fun handleError(leagues: List<League>?) {
        swipe_refresh.isRefreshing = false
        renderLeagues(leagues)
        showErrorMessage()
    }

    private fun renderLeagues(leagues: List<League>?) {
        leagues?.let {
            leagueListAdapter.submitList(it)
        }
    }

    private fun showErrorMessage() = toast(getString(R.string.error_message))

    override fun onItemSelected(position: Int, item: League) {
        Intent(this, TeamsActivity::class.java).apply {
            putExtra(EXTRA_LEAGUE_ID, item.leagueId)
            putExtra(EXTRA_LEAGUE_TITLE, item.name)
            putExtra(EXTRA_LEAGUE_BADGE_URL, item.badgeUrl)
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this@LeaguesActivity,
                recycler_leagues.findViewWithTag(item.leagueId),
                "detail"
            )
            startActivity(this, options.toBundle())
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_leagues, menu)
        val searchItem: MenuItem = menu.findItem(R.id.action_search)
        val searchView: SearchView = searchItem.actionView as SearchView
        searchView.imeOptions = EditorInfo.IME_ACTION_DONE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                leagueListAdapter.filter.filter(newText)
                return false
            }
        })
        return true
    }
}
