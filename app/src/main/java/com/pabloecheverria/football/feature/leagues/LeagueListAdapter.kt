package com.pabloecheverria.football.feature.leagues

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.pabloecheverria.football.R
import com.pabloecheverria.football.base.extension.loadImageUrl
import com.pabloecheverria.football.feature.leagues.db.League
import kotlinx.android.synthetic.main.item_league.view.*

class LeagueListAdapter(private val interaction: Interaction? = null) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    private var originalList = emptyList<League>()

    private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<League>() {

        override fun areItemsTheSame(oldItem: League, newItem: League): Boolean {
            return oldItem.leagueId == newItem.leagueId
        }

        override fun areContentsTheSame(oldItem: League, newItem: League): Boolean {
            return oldItem == newItem
        }

    }
    private val differ = AsyncListDiffer(this, DIFF_CALLBACK)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return LeagueViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_league,
                parent,
                false
            ),
            interaction
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LeagueViewHolder -> {
                holder.bind(differ.currentList.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun submitList(list: List<League>) {
        originalList = ArrayList<League>(list)
        differ.submitList(list)
    }

    class LeagueViewHolder
    constructor(
        itemView: View,
        private val interaction: Interaction?
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: League) = with(itemView) {
            itemView.setOnClickListener {
                interaction?.onItemSelected(adapterPosition, item)
            }

            itemView.title.text = item.name
            itemView.badge.loadImageUrl(item.badgeUrl)
            itemView.badge.tag = item.leagueId
        }
    }

    interface Interaction {
        fun onItemSelected(position: Int, item: League)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(input: CharSequence?): FilterResults {
                val result = FilterResults()
                if (input.isNullOrBlank()) {
                    result.values = originalList
                } else {
                    result.values = originalList.filter { it.name.contains(input, true) }
                }
                return result
            }

            override fun publishResults(input: CharSequence?, results: FilterResults?) {
                differ.submitList(results?.values as List<League>)
                notifyDataSetChanged()
            }
        }
    }
}