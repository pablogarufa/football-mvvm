package com.pabloecheverria.football.feature.teams

import com.pabloecheverria.football.base.custom.State
import com.pabloecheverria.football.feature.teams.db.Team
import kotlinx.coroutines.flow.Flow

interface TeamRepository {
    fun observeTeams(leagueId: Int): Flow<State<List<Team>>>
    fun getTeams(leagueId: Int): Flow<State<List<Team>>>
    suspend fun addTeam(teamName: String, leagueId: Int)
    suspend fun deleteTeam(team: Team)
}