package com.pabloecheverria.football.feature.leagues.network

import com.google.gson.annotations.SerializedName

data class LeagueDTO(
    @SerializedName("country_id") val country_id: Int,
    @SerializedName("country_name") val country_name: String,
    @SerializedName("league_id") val league_id: Int,
    @SerializedName("league_name") val league_name: String,
    @SerializedName("league_season") val league_season: String,
    @SerializedName("league_logo") val league_logo: String,
    @SerializedName("country_logo") val country_logo: String
)