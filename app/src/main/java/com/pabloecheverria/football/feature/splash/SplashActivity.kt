package com.pabloecheverria.football.feature.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.pabloecheverria.football.feature.leagues.LeaguesActivity
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launch {
            delay(2 * SECOND)
            Intent(this@SplashActivity, LeaguesActivity::class.java).apply {
                this@SplashActivity.startActivity(this)
                this@SplashActivity.finish()
            }
        }
    }

    companion object {
        private const val SECOND = 1000L
    }
}