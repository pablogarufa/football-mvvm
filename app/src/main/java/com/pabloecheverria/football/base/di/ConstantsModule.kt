package com.pabloecheverria.football.base.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object ConstantsModule {
    @Singleton
    @Provides
    fun providesBaseUrl(): String {
        return "https://apiv2.apifootball.com"
    }
}
