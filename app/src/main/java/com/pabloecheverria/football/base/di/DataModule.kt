package com.pabloecheverria.football.base.di

import android.content.Context
import androidx.room.Room
import com.google.firebase.firestore.FirebaseFirestore
import com.pabloecheverria.football.base.database.FootballDatabase
import com.pabloecheverria.football.base.mapper.Mapper
import com.pabloecheverria.football.base.network.FirestoreDataSource
import com.pabloecheverria.football.base.network.FootballService
import com.pabloecheverria.football.feature.leagues.LeagueRepository
import com.pabloecheverria.football.feature.leagues.LeagueRepositoryImpl
import com.pabloecheverria.football.feature.leagues.db.League
import com.pabloecheverria.football.feature.leagues.db.LeagueDao
import com.pabloecheverria.football.feature.leagues.db.LeagueMapper
import com.pabloecheverria.football.feature.leagues.network.LeagueDTO
import com.pabloecheverria.football.feature.teams.TeamMapper
import com.pabloecheverria.football.feature.teams.TeamRepository
import com.pabloecheverria.football.feature.teams.TeamRepositoryImpl
import com.pabloecheverria.football.feature.teams.db.Team
import com.pabloecheverria.football.feature.teams.db.TeamDao
import com.pabloecheverria.football.feature.teams.network.TeamDTO
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers.IO
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object DataModule {

    @Singleton
    @Provides
    fun providesCoroutinesDispatcher(): CoroutineDispatcher {
        return IO
    }

    @Singleton
    @Provides
    fun providesTeamMapper(): Mapper<TeamDTO, Team> {
        return TeamMapper()
    }

    @Singleton
    @Provides
    fun providesLeaguesMapper(): Mapper<LeagueDTO, League> {
        return LeagueMapper()
    }

    @Singleton
    @Provides
    fun providesFirebaseFireStore(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }

    @Singleton
    @Provides
    fun providesFirestoreDatasource(firebaseFirestore: FirebaseFirestore): FirestoreDataSource {
        return FirestoreDataSource(firebaseFirestore)
    }

    @Singleton
    @Provides
    fun providesFootballDatabase(@ApplicationContext context: Context): FootballDatabase {
        return Room.databaseBuilder(context, FootballDatabase::class.java, "database-teams").build()
    }

    @Singleton
    @Provides
    fun providesLeagueDao(footballDatabase: FootballDatabase): LeagueDao {
        return footballDatabase.leagueDao()
    }

    @Singleton
    @Provides
    fun providesTeamDao(footballDatabase: FootballDatabase): TeamDao {
        return footballDatabase.teamDao()
    }

    @Singleton
    @Provides
    fun providesRetrofit(baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(OkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun providesFootballService(retrofit: Retrofit): FootballService {
        return FootballService(retrofit)
    }

    @Singleton
    @Provides
    fun providesTeamsRepository(
        mapper: Mapper<TeamDTO, Team>,
        teamDao: TeamDao,
        footballService: FootballService,
        firestoreDataSource: FirestoreDataSource
    ): TeamRepository {
        return TeamRepositoryImpl(mapper, teamDao, footballService, firestoreDataSource)
    }

    @Singleton
    @Provides
    fun providesLeaguesRepository(
        mapper: Mapper<LeagueDTO, League>,
        leagueDao: LeagueDao,
        footballService: FootballService,
        firestoreDataSource: FirestoreDataSource
    ): LeagueRepository {
        return LeagueRepositoryImpl(mapper, leagueDao, footballService, firestoreDataSource)
    }
}