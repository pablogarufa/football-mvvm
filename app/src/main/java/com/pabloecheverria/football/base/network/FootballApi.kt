package com.pabloecheverria.football.base.network

import com.pabloecheverria.football.feature.leagues.network.LeagueDTO
import com.pabloecheverria.football.feature.teams.network.TeamDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface FootballApi {

    companion object {
        private const val key = "7a1cccef3b08339254cb79331ef9a73e079f106173a53554306e96d2e3c5665c"
    }

    @GET("/?APIkey=$key&action=get_teams")
    suspend fun getTeams(@Query("league_id") league_id: Int): List<TeamDTO>

    @GET("/?APIkey=$key&action=get_leagues")
    suspend fun getLeagues(): List<LeagueDTO>
}