package com.pabloecheverria.football.util

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object FakeConstantsModule {
    @Singleton
    @Provides
    fun providesBaseUrl(): String {
        return "http://localhost:8080/"
    }
}
