package com.pabloecheverria.football.feature

import android.content.Intent
import androidx.test.espresso.IdlingRegistry
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.pabloecheverria.football.base.custom.EspressoIdlingResource.getIdlingResource
import com.pabloecheverria.football.base.di.ConstantsModule
import com.pabloecheverria.football.feature.teams.TeamsActivity
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.net.HttpURLConnection.HTTP_BAD_REQUEST
import java.net.HttpURLConnection.HTTP_OK
import java.util.concurrent.TimeUnit

@UninstallModules(ConstantsModule::class)
@HiltAndroidTest
class TeamsActivityTest {
    private lateinit var mockWebServer: MockWebServer

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    val activityTestRule = ActivityTestRule(TeamsActivity::class.java, true, false)

    @Before
    fun setUp() {
        hiltRule.inject()
        // Startup mock web server
        mockWebServer = MockWebServer()
        mockWebServer.start(8080)
        // For Idling resources
        IdlingRegistry.getInstance().register(getIdlingResource())
    }

    @Test
    fun givenSuccessfulServerResponse_whenLaunchesActivity_thenShowListOfProducts() {
        mockWebServer.dispatcher = provideSuccessfulDispatcher()

        activityTestRule.launchActivity(provideIntent())

        // TODO: Review commented line because makes test fails randomly
        TeamsScreenRobot
            //.checkIsRefreshing()
            .checkToolbarDisplayedCorrectly()
            .checkRecyclerViewHasItems()
            .checkIsNotRefreshing()
    }

    @Test
    fun givenErrorServerResponse_whenLaunchesActivity_thenShowErrorToast() {
        mockWebServer.dispatcher = provideErrorDispatcher()

        activityTestRule.launchActivity(provideIntent())

        // TODO: Review commented line because makes test fails randomly
        TeamsScreenRobot
            //.checkIsRefreshing()
            .checkRecyclerViewIsEmpty()
            .checkErrorToastDisplayed(activityTestRule.activity.window.decorView)
            .checkIsNotRefreshing()
    }

    private fun provideSuccessfulDispatcher(): Dispatcher {
        return object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                val mockedResponse =
                    InstrumentationRegistry.getInstrumentation().context.assets.open("json/response_teams_ok.json")
                        .bufferedReader().use { it.readText() }
                return MockResponse().setResponseCode(HTTP_OK).setBody(mockedResponse)
                    .setBodyDelay(FAKE_LOADING_TIME_IN_MILLIS, TimeUnit.MILLISECONDS)
            }
        }
    }

    private fun provideErrorDispatcher(): Dispatcher {
        return object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse =
                MockResponse().setResponseCode(HTTP_BAD_REQUEST).setBody("empty body")
                    .setBodyDelay(FAKE_LOADING_TIME_IN_MILLIS, TimeUnit.MILLISECONDS)
        }
    }

    private fun provideIntent() = Intent().apply {
        putExtra(TeamsActivity.EXTRA_LEAGUE_ID, 1)
        putExtra(TeamsActivity.EXTRA_LEAGUE_TITLE, "Everton")
        putExtra(TeamsActivity.EXTRA_LEAGUE_BADGE_URL, "www.image.com")
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
        // For Idling resource
        IdlingRegistry.getInstance().unregister(getIdlingResource())
    }

    companion object {
        private const val FAKE_LOADING_TIME_IN_MILLIS = 1500L
    }
}
