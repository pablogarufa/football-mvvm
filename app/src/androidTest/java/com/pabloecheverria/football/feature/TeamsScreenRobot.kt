package com.pabloecheverria.football.feature

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.hasDescendant
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.pabloecheverria.football.R
import com.pabloecheverria.football.util.CustomMatchers
import com.pabloecheverria.football.util.CustomMatchers.atPosition
import com.pabloecheverria.football.util.CustomMatchers.isRefreshing
import com.pabloecheverria.football.util.CustomMatchers.withCollapsibleToolbarTitle
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not

object TeamsScreenRobot {
    fun checkIsRefreshing(): TeamsScreenRobot {
        onView(withId(R.id.swipe_refresh)).check(matches(isRefreshing()))
        return this
    }

    fun checkIsNotRefreshing(): TeamsScreenRobot {
        onView(withId(R.id.swipe_refresh)).check(matches(not(isRefreshing())))
        return this
    }

    fun checkToolbarDisplayedCorrectly(): TeamsScreenRobot {
        onView(withId(R.id.collapsing_tollbar)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.collapsing_tollbar)).check(
            matches(
                withCollapsibleToolbarTitle(`is`("Everton"))
            )
        )
        return this
    }

    fun checkRecyclerViewHasItems(): TeamsScreenRobot {
        onView(withId(R.id.recycler_teams)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.recycler_teams)).check(matches(CustomMatchers.hasItemCount(10)))
        onView(withId(R.id.recycler_teams)).check(
            matches(
                atPosition(
                    0,
                    hasDescendant(withText("Bylis"))
                )
            )
        )
        return this
    }

    fun checkRecyclerViewIsEmpty(): TeamsScreenRobot {
        onView(withId(R.id.recycler_teams)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.recycler_teams)).check(matches(CustomMatchers.hasItemCount(0)))
        return this
    }

    fun checkErrorToastDisplayed(view: View): TeamsScreenRobot {
        onView(withText(R.string.error_message))
            .inRoot(withDecorView(not(view)))
            .check(matches(ViewMatchers.isDisplayed()))
        return this
    }
}